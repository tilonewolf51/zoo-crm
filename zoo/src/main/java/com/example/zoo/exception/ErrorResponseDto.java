package com.example.zoo.exception;

import lombok.Data;

@Data
public class ErrorResponseDto {
    private String message;
}

package com.example.zoo.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Constants {

    public static final String MAMMAL = "Млекопитающее";

    public static final String BIRD = "Птица";

    @UtilityClass
    public class NameFood {
        public static final String GRAIN = "Зерно";

        public static final String CARROTS = "Морковь";

        public static final String CABBAGE = "Капуста";

        public static final String BANANA = "Банан";

        public static final String APPLE = "Яблоко";

        public static final String WATER = "Вода";

    }

    @UtilityClass
    public class Unit {
        public static final String KG = "кг";

        public static final String THING = "шт";

        public static final String LITER = "л";

    }

    @UtilityClass
    public class TypeFood {

        public static final String VEGETABLE = "Овощь";

        public static final String FRUIT = "Фрукт";

        public static final String MEAT = "Мясо";

        public static final String LIQUID = "Жидкость";
    }

    @UtilityClass
    public class TypeAnimal {

        public static final String HORSE = "Лошадь";

        public static final String EAGLE = "Орел";

        public static final String TIGER = "Тигр";

        public static final String HARE = "Заяц";

        public static final String MONKEY = "Обезьяна";
    }

}

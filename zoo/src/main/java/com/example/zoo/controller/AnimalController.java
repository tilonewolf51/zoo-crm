package com.example.zoo.controller;

import com.example.zoo.entity.Animal;
import com.example.zoo.service.AnimalService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequiredArgsConstructor
@RequestMapping("/animal")
public class AnimalController {

    private final AnimalService animalService;

    @PostMapping
    public Animal addAnimal(@RequestBody Animal animal) {
        return animalService.addAnimal(animal);
    }

    @PutMapping("/hungry/{id}")
    public Animal changeHungry(@PathVariable Long id, @RequestParam Integer value) {
        return animalService.changeHungry(id, value);
    }

    @PutMapping("/addFood/{animalId}/{foodId}")
    public Animal changeHungry(@PathVariable Long animalId, @PathVariable Long foodId) {
        return animalService.addFood(animalId, foodId);
    }

    @PutMapping("/addFoodsByIds/{animalId}")
    public Animal changeHungry(@PathVariable Long animalId, @RequestBody Set<Long> foodIds) {
        return animalService.addFoodsByIds(animalId, foodIds);
    }

    @GetMapping("/{id}")
    public Animal getAnimalById(@PathVariable Long id) {
        return animalService.findAnimalById(id);
    }

    @GetMapping("info/{id}")
    public String getInfoById(@PathVariable Long id) {
        return animalService.allInfoById(id);
    }

    @GetMapping
    public List<Animal> getAll() {
        return animalService.getAllAnimal();
    }

    @GetMapping("/plain/{id}")
    public String getWeeklyFood(@PathVariable Long id) {
        return animalService.getWeeklyRation(id);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id) {
        animalService.deleteAnimalById(id);
    }

    @DeleteMapping("/all")
    public void deleteAll() {
        animalService.deleteAll();
    }

    @DeleteMapping
    public void deleteByIds(@RequestBody Set<Long> ids) {
        animalService.deleteByIds(ids);
    }
}

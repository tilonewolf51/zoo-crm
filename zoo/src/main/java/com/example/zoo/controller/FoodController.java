package com.example.zoo.controller;

import com.example.zoo.entity.Food;
import com.example.zoo.service.FoodService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequiredArgsConstructor
@RequestMapping("/food")
public class FoodController {

    private final FoodService foodService;

    @PostMapping
    public Food addFood(@RequestBody Food food) {
        return foodService.addFood(food);
    }

    @PutMapping("/count/{id}")
    public Food changeCount(@PathVariable Long id, @RequestParam Integer value) {
        return foodService.changeHungry(id, value);
    }

    @GetMapping("/{id}")
    public Food getAnimalById(@PathVariable Long id) {
        return foodService.findFoodById(id);
    }

    @GetMapping
    public List<Food> getAll() {
        return foodService.getAllFoods();
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id) {
        foodService.deleteFoodById(id);
    }

    @DeleteMapping("/all")
    public void deleteAll() {
        foodService.deleteAll();
    }

    @DeleteMapping
    public void deleteAllByIds(@RequestBody Set<Long> ids) {
        foodService.deleteByIds(ids);
    }
}

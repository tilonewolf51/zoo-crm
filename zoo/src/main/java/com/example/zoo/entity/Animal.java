package com.example.zoo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Setter
@Getter
@NoArgsConstructor
@Table(name = "animal")
@Accessors(chain = true)
public class Animal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String nickname;

    private String type;

    private boolean predator;

    private int hungry;

    @JsonIgnore
    @ManyToMany
    @JoinTable(
            name = "food_animal",
            joinColumns = @JoinColumn(name = "animal_id"),
            inverseJoinColumns = @JoinColumn(name = "food_id"))
    private Set<Food> foods;

    public Animal(String name, String nickname, String type, boolean predator, Integer hungry, Set<Food> foods) {
        this.name = name;
        this.nickname = nickname;
        this.type = type;
        this.predator = predator;
        this.hungry = hungry;
        this.foods = foods;
    }

    @Override
    public String toString() {
        String isPredator = predator ? "да" : "нет";
        var result = "Животное с " +
                "id=" + id +
                ", являющийся='" + name + '\'' +
                ", кличка='" + nickname + '\'' +
                ", тип='" + type + '\'' +
                ", хищник=" + isPredator +
                ", которому нужно есть=" + hungry +
                ",который питается =" + foods.stream().map(Food::toString).collect(Collectors.toSet());
        return result.replace("[", "").replace("]", "");
    }
}

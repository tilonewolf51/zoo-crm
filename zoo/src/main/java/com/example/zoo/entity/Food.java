package com.example.zoo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
public class Food {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String unit;

    private String type;

    private Integer count;

    @JsonIgnore
    @ManyToMany
    @JoinTable(
            name = "food_animal",
            joinColumns = @JoinColumn(name = "food_id"),
            inverseJoinColumns = @JoinColumn(name = "animal_id"))
    private Set<Animal> animals;

    public Food(String name, String unit, String type, Integer count) {
        this.name = name;
        this.unit = unit;
        this.type = type;
        this.count = count;
    }

    @Override
    public String toString() {
        return "Еда " +
                "id=" + id +
                ", name='" + name + '\'' +
                ", unit='" + unit + '\'' +
                ", type='" + type + '\'' +
                ", count=" + count;
    }
}

package com.example.zoo.service;

import com.example.zoo.entity.Animal;
import com.example.zoo.entity.Food;
import com.example.zoo.repository.AnimalRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AnimalService {

    private final AnimalRepository animalRepository;

    private final FoodService foodService;

    /**
     * Понял это как, у нас есть переменная animal.getCountHungry() количества одной единицы еды.
     * Это значит, если animal.getCountHungry() = 5, то, чтобы быть сытым одним видом еды, нужно 5 едиц этой еды.
     */
    public String getWeeklyRation(Long id) {
        LocalDate localDate = LocalDate.now();
        var animal = findAnimalOrEntityNotFoundException(id);
        int hungry = animal.getHungry();
        String string = animal.getFoods().stream()
                .map(food -> {
                    int weeklyHungry = hungry * 7;
                    int countWithWeeklyHungry = food.getCount() - weeklyHungry;
                    if (food.getCount() < hungry || countWithWeeklyHungry < 0) {
                        return food.getName() + " - не хватит";
                    } else {
                        return food.getName() + " в количестве " + weeklyHungry + food.getUnit();
                    }
                }).collect(Collectors.toSet()).toString().replace("[", "").replace("]","");
        return "В период с " + localDate + " до " + localDate.plusDays(7) + " нужно: " + string;
    }

    public Animal findAnimalById(Long id) {
        return findAnimalOrEntityNotFoundException(id);
    }

    public List<Animal> getAllAnimal() {
        return animalRepository.findAll();
    }

    public Animal addFoodsByIds(Long animalId, Set<Long> ids) {
        Animal animal = findAnimalOrEntityNotFoundException(animalId);
        Set<Food> foods = new HashSet<>();
        ids.forEach(id -> foods.add(foodService.findFoodById(id)));
        animal.getFoods().addAll(foods);
        return animalRepository.save(animal);
    }

    public Animal addFood (Long animalId,Long foodId) {
        Animal animal = findAnimalOrEntityNotFoundException(animalId);
        Food food = foodService.findFoodById(foodId);
        animal.getFoods().add(food);
        return animalRepository.save(animal);
    }

    public void deleteAnimalById(Long id) {
        deleteOneById(id);
    }

    public void deleteByIds(Set<Long> ids) {
        ids.forEach(this::deleteOneById);
    }

    public void deleteAll(){
        animalRepository.deleteAll();
    }

    public String allInfoById(Long id) {
        return findAnimalOrEntityNotFoundException(id).toString();
    }

    public Animal addAnimal(Animal animal) {
        return animalRepository.save(animal);
    }

    public Animal changeHungry(Long id, Integer value) {
        return findAnimalOrEntityNotFoundException(id).setHungry(value);
    }

    private Animal findAnimalOrEntityNotFoundException(Long id) {
        return animalRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Animal with id %s not found", id)));
    }

    private void deleteOneById(Long id) {
        animalRepository.delete(findAnimalOrEntityNotFoundException(id));
    }
}

package com.example.zoo.service;

import com.example.zoo.entity.Food;
import com.example.zoo.repository.FoodRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class FoodService {

    private final FoodRepository foodRepository;

    public Food findFoodById(Long id) {
        return findFoodOrEntityNotFoundException(id);
    }

    public List<Food> getAllFoods() {
        return foodRepository.findAll();
    }

    public void deleteFoodById(Long id) {
        foodRepository.delete(findFoodOrEntityNotFoundException(id));
    }

    public void deleteByIds(Set<Long> ids) {
        ids.forEach(this::deleteOneById);
    }

    public void deleteAll(){
        foodRepository.deleteAll();
    }

    public Food addFood(Food food) {
       return foodRepository.save(food);
    }

    public Food changeHungry(Long id, Integer value) {
        return findFoodOrEntityNotFoundException(id).setCount(value);
    }

    private Food findFoodOrEntityNotFoundException(Long id) {
        return foodRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Animal with id %s not found", id)));
    }

    private void deleteOneById(Long id) {
        foodRepository.delete(findFoodOrEntityNotFoundException(id));
    }
}

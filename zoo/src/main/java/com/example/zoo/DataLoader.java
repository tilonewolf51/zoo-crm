package com.example.zoo;

import com.example.zoo.entity.Animal;
import com.example.zoo.entity.Food;
import com.example.zoo.repository.AnimalRepository;
import com.example.zoo.repository.FoodRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Set;

import static com.example.zoo.util.Constants.BIRD;
import static com.example.zoo.util.Constants.MAMMAL;
import static com.example.zoo.util.Constants.NameFood.*;
import static com.example.zoo.util.Constants.TypeAnimal.*;
import static com.example.zoo.util.Constants.TypeFood.*;
import static com.example.zoo.util.Constants.Unit.*;

@Component
@RequiredArgsConstructor
public class DataLoader implements CommandLineRunner {

    private final AnimalRepository animalRepository;

    private final FoodRepository foodRepository;

    @Override
    public void run(String... args) {
        if (animalRepository.findAll().isEmpty() && foodRepository.findAll().isEmpty()) {
            Food carrots = new Food(CARROTS, THING, VEGETABLE, 20);
            Food meat = new Food(MEAT, KG, MEAT, 10);
            Food banana = new Food(BANANA, THING, FRUIT, 20);
            Food apple = new Food(APPLE, THING, FRUIT, 0);
            Food grain = new Food(GRAIN, KG, VEGETABLE, 100);
            Food cabbage = new Food(CABBAGE, KG, VEGETABLE, 20);
            Food water = new Food(WATER, LITER, LIQUID, 2445777);
            Set<Food> allFood = Set.of(carrots, meat, banana, apple, grain, cabbage, water);

            foodRepository.saveAll(allFood);

            Set<Food> foodsForMonkey = Set.of(banana, apple, water);
            Set<Food> foodsFormHorse = Set.of(grain, apple, carrots);
            Set<Food> foodsFormEagle = Set.of(meat, grain, apple, water);
            Set<Food> foodsFormTiger = Set.of(meat, water);
            Set<Food> foodsFormHare = Set.of(cabbage, carrots, water);


            Set<Animal> animals = Set.of(
                    new Animal(HORSE, "Сириус", MAMMAL, false, 5, foodsFormHorse),
                    new Animal(TIGER, "Огонек", MAMMAL, true, 10, foodsFormTiger),
                    new Animal(EAGLE, "Клэр", BIRD, true, 2, foodsFormEagle),
                    new Animal(MONKEY, "Арчи", MAMMAL, false, 3, foodsForMonkey),
                    new Animal(HARE, "Мик", MAMMAL, false, 2, foodsFormHare)
            );
            animalRepository.saveAll(animals);
        }
    }

}
